//
//  ViewController.swift
//  DACN
//
//  Created by Minh Mon on 12/18/19.
//  Copyright © 2019 Minh Mon. All rights reserved.
//

import UIKit
import Then
import FirebaseStorage
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var cardView: UIImageView!
    @IBOutlet weak var chooseImageButton: UIButton!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    
    private let picker = UIImagePickerController()
    private let storage = Storage.storage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    func config() {
        chooseImageButton.do {
            $0.layer.cornerRadius = 5
        }
        cardView.do {
            $0.layer.cornerRadius = 10
            $0.layer.masksToBounds = true
        }
        picker.do {
            $0.delegate = self
            $0.allowsEditing = true
            $0.sourceType = .photoLibrary
        }
    }

    @IBAction func chooseImageTapped(_ sender: Any) {
        present(picker, animated: true, completion: nil)
    }
}

extension ViewController {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let pickerImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        cardView.image = pickerImage
        uploadImage(image: pickerImage) { (url) in
            Alamofire.request("http://localhost:3000/uploads", method: .post, parameters: ["image_url": url], encoding: JSONEncoding.default, headers: [:]).responseObject { (response: DataResponse<OCRResponse>) in
                guard let object = response.result.value else { return }
                self.nameLabel.text = object.name
                self.emailLabel.text = object.email
                self.phoneLabel.text = object.phone
                self.companyLabel.text = object.company
                print(object.sentences)
            }
        }
    
        dismiss(animated: true, completion: nil)
    }
    
    func uploadImage(image: UIImage, completion: @escaping (String) -> Void) {
        guard let data = image.jpegData(compressionQuality: 1.0) else { return }
        
        let storageRef = storage.reference()
        let cardRef = storageRef.child("images/card.jpg")

        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = cardRef.putData(data, metadata: nil) { (metadata, error) in
          guard let metadata = metadata else { return }
          // Metadata contains file metadata such as size, content-type.
          let size = metadata.size
          // You can also access to download URL after upload.
          cardRef.downloadURL { (url, error) in
            guard let downloadURL = url?.absoluteString else { return }
            completion(downloadURL)
          }
        }
    }
}

