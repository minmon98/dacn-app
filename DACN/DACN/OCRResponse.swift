//
//  OCRResponse.swift
//  DACN
//
//  Created by Minh Mon on 12/18/19.
//  Copyright © 2019 Minh Mon. All rights reserved.
//

import Foundation
import ObjectMapper

class OCRResponse: Mappable {
    var name = ""
    var email = ""
    var phone = ""
    var company = ""
    var sentences = [String]()
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        name <- map["name"]
        email <- map["email"]
        phone <- map["phone"]
        company <- map["company"]
        sentences <- map["sentences"]
    }
}
